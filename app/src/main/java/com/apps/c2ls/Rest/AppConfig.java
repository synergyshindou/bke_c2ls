package com.apps.c2ls.Rest;

import android.content.Context;
import android.support.annotation.NonNull;

import com.apps.c2ls.Utils.SessionManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.apps.c2ls.Utils.Constant.BASE_URL;
import static com.apps.c2ls.Utils.Constant.BASE_URLS;
import static com.apps.c2ls.Utils.Constant.BASE_URL_PRODUCTION;


public class AppConfig {


    public static Retrofit getRetrofit() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//        OkHttpClient client = new OkHttpClient.Builder().build();
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
//                .baseUrl(BASE_URLS)
//                .baseUrl(BASE_URL_PRODUCTION)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static Retrofit getRetrofit(Context context) {

        SessionManager sessionManager = new SessionManager(context);
        final String sToken = sessionManager.getToken();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {

                Request request = chain.request()
                        .newBuilder()
                        .addHeader("Authorization", "Bearer " + sToken)
                        .build();
                return chain.proceed(request);
            }
        }).addInterceptor(interceptor).build();
        return new Retrofit.Builder()
//                .baseUrl(BASE_URLS)
                .baseUrl(BASE_URL_PRODUCTION)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
