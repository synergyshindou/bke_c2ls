package com.apps.c2ls.Rest;

import com.apps.c2ls.Model.CitizenShipResponse;
import com.apps.c2ls.Model.CustomerEntry;
import com.apps.c2ls.Model.CustomerEntryResponse;
import com.apps.c2ls.Model.GlobalResponse;
import com.apps.c2ls.Model.HomeStayResponse;
import com.apps.c2ls.Model.LoginEntry;
import com.apps.c2ls.Model.LoginResponse;
import com.apps.c2ls.Model.MaritalResponse;
import com.apps.c2ls.Model.RegisterEntry;
import com.apps.c2ls.Model.SexResponse;
import com.apps.c2ls.Model.SimulasiEntry;
import com.apps.c2ls.Model.SimulasiResponse;
import com.apps.c2ls.Model.ZipcodeReponse;
import com.apps.c2ls.Model.PostZipcode;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ApiConfig {

    //    @FormUrlEncoded
//    @POST("AuthLogin")
//    Call<LoginResponse> login(@Field("name") String name, @Field("password") String pass);
//
//
    @GET("citizenship/retreivecitizenship")
    Call<CitizenShipResponse> getCitizen();

    @GET("homestay/retreivehomestay")
    Call<HomeStayResponse> getHomestay();

    @GET("sex/retreivesex")
    Call<SexResponse> getSex();

    @GET("marital/retreivemarital")
    Call<MaritalResponse> getMarital();

    @POST("zipcode/searchzipcode")
    Call<ZipcodeReponse> postZipcode(@Body PostZipcode postLogin);

    @POST("customer/initialentrypersonalaccountinfo")
    Call<CustomerEntryResponse> postCustomer(@Body CustomerEntry customerEntry);

    @POST("creditsimulation/calculatecreditsimulation")
    Call<SimulasiResponse> postSimulasi(@Body SimulasiEntry simulasiEntry);

    @POST("account/registeraccount")
    Call<GlobalResponse> registerAccount(@Body RegisterEntry registerEntry);

    @POST("account/login")
    Call<LoginResponse> login(@Body LoginEntry loginEntry);

}