package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataHome implements Parcelable {

    public final static Parcelable.Creator<DataHome> CREATOR = new Creator<DataHome>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataHome createFromParcel(Parcel in) {
            DataHome instance = new DataHome();
            instance.HMCODE = ((int) in.readValue((int.class.getClassLoader())));
            instance.HMDESC = ((String) in.readValue((String.class.getClassLoader())));
            instance.CDSIBS = ((String) in.readValue((String.class.getClassLoader())));
            instance.ACTIVE = ((String) in.readValue((String.class.getClassLoader())));
            instance.HMSTW = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataHome[] newArray(int size) {
            return (new DataHome[size]);
        }

    };
    @SerializedName("HM_CODE")
    @Expose
    private int HMCODE;
    @SerializedName("HM_DESC")
    @Expose
    private String HMDESC;
    @SerializedName("CD_SIBS")
    @Expose
    private String CDSIBS;
    @SerializedName("ACTIVE")
    @Expose
    private String ACTIVE;
    @SerializedName("HM_STW")
    @Expose
    private String HMSTW;

    /**
     * @return The HMCODE
     */
    public int getHMCODE() {
        return HMCODE;
    }

    /**
     * @param HMCODE The HM_CODE
     */
    public void setHMCODE(int HMCODE) {
        this.HMCODE = HMCODE;
    }

    /**
     * @return The HMDESC
     */
    public String getHMDESC() {
        return HMDESC;
    }

    /**
     * @param HMDESC The HM_DESC
     */
    public void setHMDESC(String HMDESC) {
        this.HMDESC = HMDESC;
    }

    /**
     * @return The CDSIBS
     */
    public String getCDSIBS() {
        return CDSIBS;
    }

    /**
     * @param CDSIBS The CD_SIBS
     */
    public void setCDSIBS(String CDSIBS) {
        this.CDSIBS = CDSIBS;
    }

    /**
     * @return The ACTIVE
     */
    public String getACTIVE() {
        return ACTIVE;
    }

    /**
     * @param ACTIVE The ACTIVE
     */
    public void setACTIVE(String ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    /**
     * @return The HMSTW
     */
    public String getHMSTW() {
        return HMSTW;
    }

    /**
     * @param HMSTW The HM_STW
     */
    public void setHMSTW(String HMSTW) {
        this.HMSTW = HMSTW;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(HMCODE);
        dest.writeValue(HMDESC);
        dest.writeValue(CDSIBS);
        dest.writeValue(ACTIVE);
        dest.writeValue(HMSTW);
    }

    public int describeContents() {
        return 0;
    }

}
