
package com.apps.c2ls.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ZipcodeReponse implements Parcelable
{

    @SerializedName("data")
    @Expose
    private List<DataZipcode> dataZipcode = new ArrayList<DataZipcode>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ZipcodeReponse{");
        sb.append("dataZipcode=").append(dataZipcode);
        sb.append(", response='").append(response).append('\'');
        sb.append(", responseText='").append(responseText).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public final static Parcelable.Creator<ZipcodeReponse> CREATOR = new Creator<ZipcodeReponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ZipcodeReponse createFromParcel(Parcel in) {
            ZipcodeReponse instance = new ZipcodeReponse();
            in.readList(instance.dataZipcode, (com.apps.c2ls.Model.DataZipcode.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ZipcodeReponse[] newArray(int size) {
            return (new ZipcodeReponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The dataZipcode
     */
    public List<DataZipcode> getDataZipcode() {
        return dataZipcode;
    }

    /**
     * 
     * @param dataZipcode
     *     The dataZipcode
     */
    public void setDataZipcode(List<DataZipcode> dataZipcode) {
        this.dataZipcode = dataZipcode;
    }

    /**
     * 
     * @return
     *     The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * 
     * @param responseText
     *     The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataZipcode);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return  0;
    }

}
