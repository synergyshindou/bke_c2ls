package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SimulasiResponse implements Parcelable {

    public final static Parcelable.Creator<SimulasiResponse> CREATOR = new Creator<SimulasiResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SimulasiResponse createFromParcel(Parcel in) {
            SimulasiResponse instance = new SimulasiResponse();
            in.readList(instance.dataSimulasi, (com.apps.c2ls.Model.DataSimulasi.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SimulasiResponse[] newArray(int size) {
            return (new SimulasiResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataSimulasi> dataSimulasi = new ArrayList<DataSimulasi>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataSimulasi
     */
    public List<DataSimulasi> getDataSimulasi() {
        return dataSimulasi;
    }

    /**
     * @param dataSimulasi The dataSimulasi
     */
    public void setDataSimulasi(List<DataSimulasi> dataSimulasi) {
        this.dataSimulasi = dataSimulasi;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataSimulasi);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

}
