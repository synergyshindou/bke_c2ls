package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaritalResponse implements Parcelable {

    public final static Parcelable.Creator<MaritalResponse> CREATOR = new Creator<MaritalResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaritalResponse createFromParcel(Parcel in) {
            MaritalResponse instance = new MaritalResponse();
            in.readList(instance.dataMarital, (com.apps.c2ls.Model.DataMarital.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public MaritalResponse[] newArray(int size) {
            return (new MaritalResponse[size]);
        }

    };

    @SerializedName("data")
    @Expose
    private List<DataMarital> dataMarital = new ArrayList<DataMarital>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataMarital
     */
    public List<DataMarital> getDataMarital() {
        return dataMarital;
    }

    /**
     * @param dataMarital The dataMarital
     */
    public void setDataMarital(List<DataMarital> dataMarital) {
        this.dataMarital = dataMarital;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataMarital);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

    public String serialize() {
        // Serialize this class into a JSON string using GSON
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    static public MaritalResponse create(String serializedData) {
        // Use GSON to instantiate this class using the JSON representation of the state
        Gson gson = new Gson();
        return gson.fromJson(serializedData, MaritalResponse.class);
    }

}
