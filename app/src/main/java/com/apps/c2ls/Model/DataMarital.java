package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataMarital implements Parcelable {

    public final static Parcelable.Creator<DataMarital> CREATOR = new Creator<DataMarital>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataMarital createFromParcel(Parcel in) {
            DataMarital instance = new DataMarital();
            instance.MARITALID = ((String) in.readValue((String.class.getClassLoader())));
            instance.MARITALDESC = ((String) in.readValue((String.class.getClassLoader())));
            instance.ACTIVE = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataMarital[] newArray(int size) {
            return (new DataMarital[size]);
        }

    };
    @SerializedName("MARITALID")
    @Expose
    private String MARITALID;
    @SerializedName("MARITALDESC")
    @Expose
    private String MARITALDESC;
    @SerializedName("ACTIVE")
    @Expose
    private String ACTIVE;

    /**
     * @return The MARITALID
     */
    public String getMARITALID() {
        return MARITALID;
    }

    /**
     * @param MARITALID The MARITALID
     */
    public void setMARITALID(String MARITALID) {
        this.MARITALID = MARITALID;
    }

    /**
     * @return The MARITALDESC
     */
    public String getMARITALDESC() {
        return MARITALDESC;
    }

    /**
     * @param MARITALDESC The MARITALDESC
     */
    public void setMARITALDESC(String MARITALDESC) {
        this.MARITALDESC = MARITALDESC;
    }

    /**
     * @return The ACTIVE
     */
    public String getACTIVE() {
        return ACTIVE;
    }

    /**
     * @param ACTIVE The ACTIVE
     */
    public void setACTIVE(String ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(MARITALID);
        dest.writeValue(MARITALDESC);
        dest.writeValue(ACTIVE);
    }

    public int describeContents() {
        return 0;
    }

}
