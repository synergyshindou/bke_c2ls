package com.apps.c2ls.Model;

/**
 * Created by Dell_Cleva on 12/04/2019.
 */

public class CustomerEntry {

    String TITLEBEFORENAME; //",
    String FIRSTNAME; //Abdul",
    String LASTNAME; //",
    String ALIASNAME; //",
    String ADDR1; //Jakarta",
    String ADDR2; //",
    String ADDR3; //Desc zipcode",
    String CITY; //123",
    String ZIPCODE; //342342",
    String HOMESTA; //1",
    int MULAIMENETAPMM;//0,
    int MULAIMENETAPYY;//0,
    String PHNAREA; //2",
    String PHNNUM; //2",
    String FAXAREA; //2",
    String FAXEXT; //2",
    String FAXNUM; //2",
    String POB; //jakarta",
    String DOB; //2019-04-10",
    String MARITAL; //id",
    String SEX; //id",
    String CITIZENSHIP; //id",
    int CHILDREN;//0,
    String NOKARTUKELUARGA; //1232312",
    String IDCARDNUM; //1234567890101",
    String IDCARDEXP; //9999-12-31",
    String KTPADDR1; //Jakarta",
    String KTPADDR2; //Timur",
    String KTPADDR3; //",
    String KTPCITY; //id",
    String KTPZIPCODE; //342342",
    String JNSALAMAT_P; //id"

    public CustomerEntry(String TITLEBEFORENAME, String FIRSTNAME, String LASTNAME, String ALIASNAME,
                         String ADDR1, String ADDR2, String ADDR3, String CITY, String ZIPCODE, String HOMESTA,
                         int MULAIMENETAPMM, int MULAIMENETAPYY, String PHNAREA, String PHNNUM, String FAXAREA,
                         String FAXEXT, String FAXNUM, String POB, String DOB, String MARITAL, String SEX,
                         String CITIZENSHIP, int CHILDREN, String NOKARTUKELUARGA, String IDCARDNUM,
                         String IDCARDEXP, String KTPADDR1, String KTPADDR2, String KTPADDR3, String KTPCITY,
                         String KTPZIPCODE, String JNSALAMAT_P) {
        this.TITLEBEFORENAME = TITLEBEFORENAME;
        this.FIRSTNAME = FIRSTNAME;
        this.LASTNAME = LASTNAME;
        this.ALIASNAME = ALIASNAME;
        this.ADDR1 = ADDR1;
        this.ADDR2 = ADDR2;
        this.ADDR3 = ADDR3;
        this.CITY = CITY;
        this.ZIPCODE = ZIPCODE;
        this.HOMESTA = HOMESTA;
        this.MULAIMENETAPMM = MULAIMENETAPMM;
        this.MULAIMENETAPYY = MULAIMENETAPYY;
        this.PHNAREA = PHNAREA;
        this.PHNNUM = PHNNUM;
        this.FAXAREA = FAXAREA;
        this.FAXEXT = FAXEXT;
        this.FAXNUM = FAXNUM;
        this.POB = POB;
        this.DOB = DOB;
        this.MARITAL = MARITAL;
        this.SEX = SEX;
        this.CITIZENSHIP = CITIZENSHIP;
        this.CHILDREN = CHILDREN;
        this.NOKARTUKELUARGA = NOKARTUKELUARGA;
        this.IDCARDNUM = IDCARDNUM;
        this.IDCARDEXP = IDCARDEXP;
        this.KTPADDR1 = KTPADDR1;
        this.KTPADDR2 = KTPADDR2;
        this.KTPADDR3 = KTPADDR3;
        this.KTPCITY = KTPCITY;
        this.KTPZIPCODE = KTPZIPCODE;
        this.JNSALAMAT_P = JNSALAMAT_P;
    }

    public String getTITLEBEFORENAME() {
        return TITLEBEFORENAME;
    }

    public void setTITLEBEFORENAME(String TITLEBEFORENAME) {
        this.TITLEBEFORENAME = TITLEBEFORENAME;
    }

    public String getFIRSTNAME() {
        return FIRSTNAME;
    }

    public void setFIRSTNAME(String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }

    public String getLASTNAME() {
        return LASTNAME;
    }

    public void setLASTNAME(String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }

    public String getALIASNAME() {
        return ALIASNAME;
    }

    public void setALIASNAME(String ALIASNAME) {
        this.ALIASNAME = ALIASNAME;
    }

    public String getADDR1() {
        return ADDR1;
    }

    public void setADDR1(String ADDR1) {
        this.ADDR1 = ADDR1;
    }

    public String getADDR2() {
        return ADDR2;
    }

    public void setADDR2(String ADDR2) {
        this.ADDR2 = ADDR2;
    }

    public String getADDR3() {
        return ADDR3;
    }

    public void setADDR3(String ADDR3) {
        this.ADDR3 = ADDR3;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getZIPCODE() {
        return ZIPCODE;
    }

    public void setZIPCODE(String ZIPCODE) {
        this.ZIPCODE = ZIPCODE;
    }

    public String getHOMESTA() {
        return HOMESTA;
    }

    public void setHOMESTA(String HOMESTA) {
        this.HOMESTA = HOMESTA;
    }

    public int getMULAIMENETAPMM() {
        return MULAIMENETAPMM;
    }

    public void setMULAIMENETAPMM(int MULAIMENETAPMM) {
        this.MULAIMENETAPMM = MULAIMENETAPMM;
    }

    public int getMULAIMENETAPYY() {
        return MULAIMENETAPYY;
    }

    public void setMULAIMENETAPYY(int MULAIMENETAPYY) {
        this.MULAIMENETAPYY = MULAIMENETAPYY;
    }

    public String getPHNAREA() {
        return PHNAREA;
    }

    public void setPHNAREA(String PHNAREA) {
        this.PHNAREA = PHNAREA;
    }

    public String getPHNNUM() {
        return PHNNUM;
    }

    public void setPHNNUM(String PHNNUM) {
        this.PHNNUM = PHNNUM;
    }

    public String getFAXAREA() {
        return FAXAREA;
    }

    public void setFAXAREA(String FAXAREA) {
        this.FAXAREA = FAXAREA;
    }

    public String getFAXEXT() {
        return FAXEXT;
    }

    public void setFAXEXT(String FAXEXT) {
        this.FAXEXT = FAXEXT;
    }

    public String getFAXNUM() {
        return FAXNUM;
    }

    public void setFAXNUM(String FAXNUM) {
        this.FAXNUM = FAXNUM;
    }

    public String getPOB() {
        return POB;
    }

    public void setPOB(String POB) {
        this.POB = POB;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMARITAL() {
        return MARITAL;
    }

    public void setMARITAL(String MARITAL) {
        this.MARITAL = MARITAL;
    }

    public String getSEX() {
        return SEX;
    }

    public void setSEX(String SEX) {
        this.SEX = SEX;
    }

    public String getCITIZENSHIP() {
        return CITIZENSHIP;
    }

    public void setCITIZENSHIP(String CITIZENSHIP) {
        this.CITIZENSHIP = CITIZENSHIP;
    }

    public int getCHILDREN() {
        return CHILDREN;
    }

    public void setCHILDREN(int CHILDREN) {
        this.CHILDREN = CHILDREN;
    }

    public String getNOKARTUKELUARGA() {
        return NOKARTUKELUARGA;
    }

    public void setNOKARTUKELUARGA(String NOKARTUKELUARGA) {
        this.NOKARTUKELUARGA = NOKARTUKELUARGA;
    }

    public String getIDCARDNUM() {
        return IDCARDNUM;
    }

    public void setIDCARDNUM(String IDCARDNUM) {
        this.IDCARDNUM = IDCARDNUM;
    }

    public String getIDCARDEXP() {
        return IDCARDEXP;
    }

    public void setIDCARDEXP(String IDCARDEXP) {
        this.IDCARDEXP = IDCARDEXP;
    }

    public String getKTPADDR1() {
        return KTPADDR1;
    }

    public void setKTPADDR1(String KTPADDR1) {
        this.KTPADDR1 = KTPADDR1;
    }

    public String getKTPADDR2() {
        return KTPADDR2;
    }

    public void setKTPADDR2(String KTPADDR2) {
        this.KTPADDR2 = KTPADDR2;
    }

    public String getKTPADDR3() {
        return KTPADDR3;
    }

    public void setKTPADDR3(String KTPADDR3) {
        this.KTPADDR3 = KTPADDR3;
    }

    public String getKTPCITY() {
        return KTPCITY;
    }

    public void setKTPCITY(String KTPCITY) {
        this.KTPCITY = KTPCITY;
    }

    public String getKTPZIPCODE() {
        return KTPZIPCODE;
    }

    public void setKTPZIPCODE(String KTPZIPCODE) {
        this.KTPZIPCODE = KTPZIPCODE;
    }

    public String getJNSALAMAT_P() {
        return JNSALAMAT_P;
    }

    public void setJNSALAMAT_P(String JNSALAMAT_P) {
        this.JNSALAMAT_P = JNSALAMAT_P;
    }
}
