package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSex implements Parcelable {

    public final static Parcelable.Creator<DataSex> CREATOR = new Creator<DataSex>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataSex createFromParcel(Parcel in) {
            DataSex instance = new DataSex();
            instance.SEXID = ((String) in.readValue((String.class.getClassLoader())));
            instance.SEXDESC = ((String) in.readValue((String.class.getClassLoader())));
            instance.ACTIVE = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataSex[] newArray(int size) {
            return (new DataSex[size]);
        }

    };
    @SerializedName("SEXID")
    @Expose
    private String SEXID;
    @SerializedName("SEXDESC")
    @Expose
    private String SEXDESC;
    @SerializedName("ACTIVE")
    @Expose
    private String ACTIVE;

    /**
     * @return The SEXID
     */
    public String getSEXID() {
        return SEXID;
    }

    /**
     * @param SEXID The SEXID
     */
    public void setSEXID(String SEXID) {
        this.SEXID = SEXID;
    }

    /**
     * @return The SEXDESC
     */
    public String getSEXDESC() {
        return SEXDESC;
    }

    /**
     * @param SEXDESC The SEXDESC
     */
    public void setSEXDESC(String SEXDESC) {
        this.SEXDESC = SEXDESC;
    }

    /**
     * @return The ACTIVE
     */
    public String getACTIVE() {
        return ACTIVE;
    }

    /**
     * @param ACTIVE The ACTIVE
     */
    public void setACTIVE(String ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SEXID);
        dest.writeValue(SEXDESC);
        dest.writeValue(ACTIVE);
    }

    public int describeContents() {
        return 0;
    }

}
