package com.apps.c2ls.Model;

/**
 * Created by Dell_Cleva on 15/04/2019.
 */

public class SimulasiEntry {
    int calculationType;// 1,
    int interestRateType;// 2,
    int plafond;// 100000000,
    int installment;// 2000000,
    double interestRate;// 3.5,
    int timePeriod;// 36

    public SimulasiEntry(int calculationType, int interestRateType, int plafond, int installment, double interestRate, int timePeriod) {
        this.calculationType = calculationType;
        this.interestRateType = interestRateType;
        this.plafond = plafond;
        this.installment = installment;
        this.interestRate = interestRate;
        this.timePeriod = timePeriod;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SimulasiEntry{");
        sb.append("calculationType=").append(calculationType);
        sb.append(", interestRateType=").append(interestRateType);
        sb.append(", plafond=").append(plafond);
        sb.append(", installment=").append(installment);
        sb.append(", interestRate=").append(interestRate);
        sb.append(", timePeriod=").append(timePeriod);
        sb.append('}');
        return sb.toString();
    }

    public int getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(int calculationType) {
        this.calculationType = calculationType;
    }

    public int getInterestRateType() {
        return interestRateType;
    }

    public void setInterestRateType(int interestRateType) {
        this.interestRateType = interestRateType;
    }

    public int getPlafond() {
        return plafond;
    }

    public void setPlafond(int plafond) {
        this.plafond = plafond;
    }

    public int getInstallment() {
        return installment;
    }

    public void setInstallment(int installment) {
        this.installment = installment;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public int getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }
}
