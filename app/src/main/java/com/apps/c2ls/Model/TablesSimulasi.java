
package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TablesSimulasi implements Parcelable
{

    @SerializedName("BULANKE")
    @Expose
    private int BULANKE;
    @SerializedName("ANGSURANPOKOK")
    @Expose
    private double ANGSURANPOKOK;
    @SerializedName("ANGSURANBUNGA")
    @Expose
    private double ANGSURANBUNGA;
    @SerializedName("TOTALANGSURAN")
    @Expose
    private double TOTALANGSURAN;
    @SerializedName("SISAPINJAMAN")
    @Expose
    private double SISAPINJAMAN;
    public final static Parcelable.Creator<TablesSimulasi> CREATOR = new Creator<TablesSimulasi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TablesSimulasi createFromParcel(Parcel in) {
            TablesSimulasi instance = new TablesSimulasi();
            instance.BULANKE = ((int) in.readValue((int.class.getClassLoader())));
            instance.ANGSURANPOKOK = ((double) in.readValue((double.class.getClassLoader())));
            instance.ANGSURANBUNGA = ((double) in.readValue((double.class.getClassLoader())));
            instance.TOTALANGSURAN = ((double) in.readValue((double.class.getClassLoader())));
            instance.SISAPINJAMAN = ((double) in.readValue((double.class.getClassLoader())));
            return instance;
        }

        public TablesSimulasi[] newArray(int size) {
            return (new TablesSimulasi[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The BULANKE
     */
    public int getBULANKE() {
        return BULANKE;
    }

    /**
     * 
     * @param BULANKE
     *     The BULANKE
     */
    public void setBULANKE(int BULANKE) {
        this.BULANKE = BULANKE;
    }

    /**
     * 
     * @return
     *     The ANGSURANPOKOK
     */
    public double getANGSURANPOKOK() {
        return ANGSURANPOKOK;
    }

    /**
     * 
     * @param ANGSURANPOKOK
     *     The ANGSURANPOKOK
     */
    public void setANGSURANPOKOK(double ANGSURANPOKOK) {
        this.ANGSURANPOKOK = ANGSURANPOKOK;
    }

    /**
     * 
     * @return
     *     The ANGSURANBUNGA
     */
    public double getANGSURANBUNGA() {
        return ANGSURANBUNGA;
    }

    /**
     * 
     * @param ANGSURANBUNGA
     *     The ANGSURANBUNGA
     */
    public void setANGSURANBUNGA(double ANGSURANBUNGA) {
        this.ANGSURANBUNGA = ANGSURANBUNGA;
    }

    /**
     * 
     * @return
     *     The TOTALANGSURAN
     */
    public double getTOTALANGSURAN() {
        return TOTALANGSURAN;
    }

    /**
     * 
     * @param TOTALANGSURAN
     *     The TOTALANGSURAN
     */
    public void setTOTALANGSURAN(double TOTALANGSURAN) {
        this.TOTALANGSURAN = TOTALANGSURAN;
    }

    /**
     * 
     * @return
     *     The SISAPINJAMAN
     */
    public double getSISAPINJAMAN() {
        return SISAPINJAMAN;
    }

    /**
     * 
     * @param SISAPINJAMAN
     *     The SISAPINJAMAN
     */
    public void setSISAPINJAMAN(double SISAPINJAMAN) {
        this.SISAPINJAMAN = SISAPINJAMAN;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(BULANKE);
        dest.writeValue(ANGSURANPOKOK);
        dest.writeValue(ANGSURANBUNGA);
        dest.writeValue(TOTALANGSURAN);
        dest.writeValue(SISAPINJAMAN);
    }

    public int describeContents() {
        return  0;
    }

}
