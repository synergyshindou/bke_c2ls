package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SexResponse implements Parcelable {

    public final static Parcelable.Creator<SexResponse> CREATOR = new Creator<SexResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SexResponse createFromParcel(Parcel in) {
            SexResponse instance = new SexResponse();
            in.readList(instance.dataSex, (com.apps.c2ls.Model.DataSex.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SexResponse[] newArray(int size) {
            return (new SexResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataSex> dataSex = new ArrayList<DataSex>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The dataSex
     */
    public List<DataSex> getDataSex() {
        return dataSex;
    }

    /**
     * @param dataSex The dataSex
     */
    public void setDataSex(List<DataSex> dataSex) {
        this.dataSex = dataSex;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataSex);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

    public String serialize() {
        // Serialize this class into a JSON string using GSON
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    static public SexResponse create(String serializedData) {
        // Use GSON to instantiate this class using the JSON representation of the state
        Gson gson = new Gson();
        return gson.fromJson(serializedData, SexResponse.class);
    }
}
