package com.apps.c2ls.Model;

/**
 * Created by Dell_Cleva on 18/04/2019.
 */

public class RegisterAccountEntry {
//    {
//        "Username": "rakishmua",
//            "Email": "rakishmu@gmail.com",
//            "Password": "test01",
//            "ConfirmPassword": "test01"
//    }

    String Username;
    String Email;
    String Password;
    String ConfirmPassword;

    public RegisterAccountEntry(String username, String email, String password, String confirmPassword) {
        Username = username;
        Email = email;
        Password = password;
        ConfirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RegisterAccountEntry{");
        sb.append("Username='").append(Username).append('\'');
        sb.append(", Email='").append(Email).append('\'');
        sb.append(", Password='").append(Password).append('\'');
        sb.append(", ConfirmPassword='").append(ConfirmPassword).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }
}
