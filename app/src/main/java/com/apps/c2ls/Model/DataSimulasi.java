package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DataSimulasi implements Parcelable {

    public final static Parcelable.Creator<DataSimulasi> CREATOR = new Creator<DataSimulasi>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataSimulasi createFromParcel(Parcel in) {
            DataSimulasi instance = new DataSimulasi();
            instance.calculationType = ((int) in.readValue((int.class.getClassLoader())));
            instance.interestRateType = ((int) in.readValue((int.class.getClassLoader())));
            instance.plafond = ((double) in.readValue((double.class.getClassLoader())));
            instance.installment = ((double) in.readValue((double.class.getClassLoader())));
            instance.interestRate = ((double) in.readValue((double.class.getClassLoader())));
            instance.timePeriod = ((int) in.readValue((int.class.getClassLoader())));
            instance.jumlAngsPokok = ((double) in.readValue((double.class.getClassLoader())));
            instance.jumlAngsBunga = ((double) in.readValue((double.class.getClassLoader())));
            instance.jumlTotAngsuran = ((double) in.readValue((double.class.getClassLoader())));
            in.readList(instance.tablesSimulasi, (com.apps.c2ls.Model.TablesSimulasi.class.getClassLoader()));
            return instance;
        }

        public DataSimulasi[] newArray(int size) {
            return (new DataSimulasi[size]);
        }

    };
    @SerializedName("calculationType")
    @Expose
    private int calculationType;
    @SerializedName("interestRateType")
    @Expose
    private int interestRateType;
    @SerializedName("plafond")
    @Expose
    private double plafond;
    @SerializedName("installment")
    @Expose
    private double installment;
    @SerializedName("interestRate")
    @Expose
    private double interestRate;
    @SerializedName("timePeriod")
    @Expose
    private int timePeriod;
    @SerializedName("jumlAngsPokok")
    @Expose
    private double jumlAngsPokok;
    @SerializedName("jumlAngsBunga")
    @Expose
    private double jumlAngsBunga;
    @SerializedName("jumlTotAngsuran")
    @Expose
    private double jumlTotAngsuran;
    @SerializedName("tables")
    @Expose
    private List<TablesSimulasi> tablesSimulasi = new ArrayList<TablesSimulasi>();

    /**
     * @return The calculationType
     */
    public int getCalculationType() {
        return calculationType;
    }

    /**
     * @param calculationType The calculationType
     */
    public void setCalculationType(int calculationType) {
        this.calculationType = calculationType;
    }

    /**
     * @return The interestRateType
     */
    public int getInterestRateType() {
        return interestRateType;
    }

    /**
     * @param interestRateType The interestRateType
     */
    public void setInterestRateType(int interestRateType) {
        this.interestRateType = interestRateType;
    }

    /**
     * @return The plafond
     */
    public double getPlafond() {
        return plafond;
    }

    /**
     * @param plafond The plafond
     */
    public void setPlafond(double plafond) {
        this.plafond = plafond;
    }

    /**
     * @return The installment
     */
    public double getInstallment() {
        return installment;
    }

    /**
     * @param installment The installment
     */
    public void setInstallment(double installment) {
        this.installment = installment;
    }

    /**
     * @return The interestRate
     */
    public double getInterestRate() {
        return interestRate;
    }

    /**
     * @param interestRate The interestRate
     */
    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * @return The timePeriod
     */
    public int getTimePeriod() {
        return timePeriod;
    }

    /**
     * @param timePeriod The timePeriod
     */
    public void setTimePeriod(int timePeriod) {
        this.timePeriod = timePeriod;
    }

    /**
     * @return The jumlAngsPokok
     */
    public double getJumlAngsPokok() {
        return jumlAngsPokok;
    }

    /**
     * @param jumlAngsPokok The jumlAngsPokok
     */
    public void setJumlAngsPokok(double jumlAngsPokok) {
        this.jumlAngsPokok = jumlAngsPokok;
    }

    /**
     * @return The jumlAngsBunga
     */
    public double getJumlAngsBunga() {
        return jumlAngsBunga;
    }

    /**
     * @param jumlAngsBunga The jumlAngsBunga
     */
    public void setJumlAngsBunga(double jumlAngsBunga) {
        this.jumlAngsBunga = jumlAngsBunga;
    }

    /**
     * @return The jumlTotAngsuran
     */
    public double getJumlTotAngsuran() {
        return jumlTotAngsuran;
    }

    /**
     * @param jumlTotAngsuran The jumlTotAngsuran
     */
    public void setJumlTotAngsuran(double jumlTotAngsuran) {
        this.jumlTotAngsuran = jumlTotAngsuran;
    }

    /**
     * @return The tablesSimulasi
     */
    public List<TablesSimulasi> getTablesSimulasi() {
        return tablesSimulasi;
    }

    /**
     * @param tablesSimulasi The tablesSimulasi
     */
    public void setTablesSimulasi(List<TablesSimulasi> tablesSimulasi) {
        this.tablesSimulasi = tablesSimulasi;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(calculationType);
        dest.writeValue(interestRateType);
        dest.writeValue(plafond);
        dest.writeValue(installment);
        dest.writeValue(interestRate);
        dest.writeValue(timePeriod);
        dest.writeValue(jumlAngsPokok);
        dest.writeValue(jumlAngsBunga);
        dest.writeValue(jumlTotAngsuran);
        dest.writeList(tablesSimulasi);
    }

    public int describeContents() {
        return 0;
    }

}
