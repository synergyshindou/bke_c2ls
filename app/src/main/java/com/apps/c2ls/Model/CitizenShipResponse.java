package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CitizenShipResponse implements Parcelable {

    public final static Parcelable.Creator<CitizenShipResponse> CREATOR = new Creator<CitizenShipResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CitizenShipResponse createFromParcel(Parcel in) {
            CitizenShipResponse instance = new CitizenShipResponse();
            in.readList(instance.dataCitizen, (DataCitizen.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CitizenShipResponse[] newArray(int size) {
            return (new CitizenShipResponse[size]);
        }

    };
    @SerializedName("data")
    @Expose
    private List<DataCitizen> dataCitizen = new ArrayList<DataCitizen>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;

    /**
     * @return The datacitizen
     */
    public List<DataCitizen> getDataCitizen() {
        return dataCitizen;
    }

    /**
     * @param dataCitizen The datacitizen
     */
    public void setDataCitizen(List<DataCitizen> dataCitizen) {
        this.dataCitizen = dataCitizen;
    }

    /**
     * @return The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * @param responseText The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataCitizen);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return 0;
    }

    public String serialize() {
        // Serialize this class into a JSON string using GSON
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    static public CitizenShipResponse create(String serializedData) {
        // Use GSON to instantiate this class using the JSON representation of the state
        Gson gson = new Gson();
        return gson.fromJson(serializedData, CitizenShipResponse.class);
    }
}
