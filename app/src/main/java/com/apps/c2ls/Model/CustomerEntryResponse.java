
package com.apps.c2ls.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerEntryResponse implements Parcelable
{

    @SerializedName("dataResponse")
    @Expose
    private List<Object> dataResponse = new ArrayList<Object>();
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("responseText")
    @Expose
    private String responseText;
    public final static Parcelable.Creator<CustomerEntryResponse> CREATOR = new Creator<CustomerEntryResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CustomerEntryResponse createFromParcel(Parcel in) {
            CustomerEntryResponse instance = new CustomerEntryResponse();
            in.readList(instance.dataResponse, (java.lang.Object.class.getClassLoader()));
            instance.response = ((String) in.readValue((String.class.getClassLoader())));
            instance.responseText = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CustomerEntryResponse[] newArray(int size) {
            return (new CustomerEntryResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The dataResponse
     */
    public List<Object> getDataResponse() {
        return dataResponse;
    }

    /**
     * 
     * @param dataResponse
     *     The dataResponse
     */
    public void setDataResponse(List<Object> dataResponse) {
        this.dataResponse = dataResponse;
    }

    /**
     * 
     * @return
     *     The response
     */
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * 
     * @param responseText
     *     The responseText
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(dataResponse);
        dest.writeValue(response);
        dest.writeValue(responseText);
    }

    public int describeContents() {
        return  0;
    }

}
