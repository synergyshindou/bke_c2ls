package com.apps.c2ls.Model;

/**
 * Created by Dell_Cleva on 23/04/2019.
 */

public class LoginEntry {
    String Username;
    String Password;

    public LoginEntry() {
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
