package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataZipcode implements Parcelable {

    public final static Parcelable.Creator<DataZipcode> CREATOR = new Creator<DataZipcode>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataZipcode createFromParcel(Parcel in) {
            DataZipcode instance = new DataZipcode();
            instance.ZIPCODE = ((String) in.readValue((String.class.getClassLoader())));
            instance.CITYID = ((String) in.readValue((String.class.getClassLoader())));
            instance.CITYNAME = ((String) in.readValue((String.class.getClassLoader())));
            instance.DESCRIPTION = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataZipcode[] newArray(int size) {
            return (new DataZipcode[size]);
        }

    };
    @SerializedName("ZIPCODE")
    @Expose
    private String ZIPCODE;
    @SerializedName("CITYID")
    @Expose
    private String CITYID;
    @SerializedName("CITYNAME")
    @Expose
    private String CITYNAME;
    @SerializedName("DESCRIPTION")
    @Expose
    private String DESCRIPTION;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DataZipcode{");
        sb.append("ZIPCODE='").append(ZIPCODE).append('\'');
        sb.append(", CITYID='").append(CITYID).append('\'');
        sb.append(", CITYNAME='").append(CITYNAME).append('\'');
        sb.append(", DESCRIPTION='").append(DESCRIPTION).append('\'');
        sb.append('}');
        return sb.toString();
    }

    /**
     * @return The ZIPCODE
     */
    public String getZIPCODE() {
        return ZIPCODE;
    }

    /**
     * @param ZIPCODE The ZIPCODE
     */
    public void setZIPCODE(String ZIPCODE) {
        this.ZIPCODE = ZIPCODE;
    }

    /**
     * @return The CITYID
     */
    public String getCITYID() {
        return CITYID;
    }

    /**
     * @param CITYID The CITYID
     */
    public void setCITYID(String CITYID) {
        this.CITYID = CITYID;
    }

    /**
     * @return The CITYNAME
     */
    public String getCITYNAME() {
        return CITYNAME;
    }

    /**
     * @param CITYNAME The CITYNAME
     */
    public void setCITYNAME(String CITYNAME) {
        this.CITYNAME = CITYNAME;
    }

    /**
     * @return The DESCRIPTION
     */
    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    /**
     * @param DESCRIPTION The DESCRIPTION
     */
    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ZIPCODE);
        dest.writeValue(CITYID);
        dest.writeValue(CITYNAME);
        dest.writeValue(DESCRIPTION);
    }

    public int describeContents() {
        return 0;
    }

}
