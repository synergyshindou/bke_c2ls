package com.apps.c2ls.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataCitizen implements Parcelable {

    public final static Parcelable.Creator<DataCitizen> CREATOR = new Creator<DataCitizen>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataCitizen createFromParcel(Parcel in) {
            DataCitizen instance = new DataCitizen();
            instance.CITIZENID = ((String) in.readValue((String.class.getClassLoader())));
            instance.CITIZENDESC = ((String) in.readValue((String.class.getClassLoader())));
            instance.ACTIVE = ((String) in.readValue((String.class.getClassLoader())));
            instance.CDSIBS = ((Object) in.readValue((Object.class.getClassLoader())));
            return instance;
        }

        public DataCitizen[] newArray(int size) {
            return (new DataCitizen[size]);
        }

    };
    @SerializedName("CITIZENID")
    @Expose
    private String CITIZENID;
    @SerializedName("CITIZENDESC")
    @Expose
    private String CITIZENDESC;
    @SerializedName("ACTIVE")
    @Expose
    private String ACTIVE;
    @SerializedName("CD_SIBS")
    @Expose
    private Object CDSIBS;

    /**
     * @return The CITIZENID
     */
    public String getCITIZENID() {
        return CITIZENID;
    }

    /**
     * @param CITIZENID The CITIZENID
     */
    public void setCITIZENID(String CITIZENID) {
        this.CITIZENID = CITIZENID;
    }

    /**
     * @return The CITIZENDESC
     */
    public String getCITIZENDESC() {
        return CITIZENDESC;
    }

    /**
     * @param CITIZENDESC The CITIZENDESC
     */
    public void setCITIZENDESC(String CITIZENDESC) {
        this.CITIZENDESC = CITIZENDESC;
    }

    /**
     * @return The ACTIVE
     */
    public String getACTIVE() {
        return ACTIVE;
    }

    /**
     * @param ACTIVE The ACTIVE
     */
    public void setACTIVE(String ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    /**
     * @return The CDSIBS
     */
    public Object getCDSIBS() {
        return CDSIBS;
    }

    /**
     * @param CDSIBS The CD_SIBS
     */
    public void setCDSIBS(Object CDSIBS) {
        this.CDSIBS = CDSIBS;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(CITIZENID);
        dest.writeValue(CITIZENDESC);
        dest.writeValue(ACTIVE);
        dest.writeValue(CDSIBS);
    }

    public int describeContents() {
        return 0;
    }

}
