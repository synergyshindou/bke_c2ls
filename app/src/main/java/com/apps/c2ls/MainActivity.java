package com.apps.c2ls;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.apps.c2ls.Adapter.MyTableViewAdapter;
import com.apps.c2ls.Model.Cell;
import com.apps.c2ls.Model.CitizenShipResponse;
import com.apps.c2ls.Model.ColumnHeader;
import com.apps.c2ls.Model.CustomerEntry;
import com.apps.c2ls.Model.CustomerEntryResponse;
import com.apps.c2ls.Model.DataCitizen;
import com.apps.c2ls.Model.DataMarital;
import com.apps.c2ls.Model.DataSex;
import com.apps.c2ls.Model.DataZipcode;
import com.apps.c2ls.Model.MaritalResponse;
import com.apps.c2ls.Model.RowHeader;
import com.apps.c2ls.Model.SexResponse;
import com.apps.c2ls.Model.SimulasiEntry;
import com.apps.c2ls.Model.SimulasiResponse;
import com.apps.c2ls.Model.TablesSimulasi;
import com.apps.c2ls.Model.ZipcodeReponse;
import com.apps.c2ls.Model.PostZipcode;
import com.apps.c2ls.Rest.ApiConfig;
import com.apps.c2ls.Rest.AppConfig;
import com.apps.c2ls.Utils.Method;
import com.apps.c2ls.Utils.SessionManager;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.filter.Filter;
import com.evrencoskun.tableview.pagination.Pagination;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    EditText etNama, etAlamat, etNoHp, etPOB, etZipcode, etDOB, etNoKTP, etAlamatKTP, etZipcodeKTP,
            etTglExpKTP, etPlafond, etInstallment;

    TextInputEditText etJangkaWaktu, etSukuBunga;

    TextView tvPemohon, tvKTP, tvKota, tvDOB, tvKotaKTP, tvTglExpKTP;

    View llPemohon, llKTP, llMain, llPengajuan, llSimulasi;

    ScrollView llScroll;

    MaterialSpinner spinStatus,spinSex,spinCitizen;

    CheckBox cbTgl, cbAlamat;

    RadioGroup radioGroup, rgSuku;

    ProgressBar progressBar;

    ApiConfig restAPI;

    List<DataZipcode> dataZipcodes;
    List<DataMarital> dataMaritals;
    List<DataSex> dataSexes;
    List<DataCitizen> dataCitizens;

    int back = 0;

    String idMarital = "", idSex = "", idCitizen = "", sAddr3 = "", sCity="", sAddr3KTP = "", sCityKTP ="";

    SessionManager sessionManager;

    android.app.AlertDialog b;

    android.app.AlertDialog.Builder dialogBuilder;

    private AbstractTableAdapter mTableViewAdapter;
    private TableView mTableView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(this);

        initToolbar("Permohonan Kredit");

        initView();

        initOnclick();

        restAPI = AppConfig.getRetrofit(this).create(ApiConfig.class);

        getMarital();

        getSex();

        getCitizen();

    }

    @Override
    public void onBackPressed() {
        if (back == 1){
            llPengajuan.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
//            llMain.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
            back = 0;
            initToolbar("Permohonan Kredit");
        } else if (back == 2){
            llSimulasi.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
//            llMain.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
            back = 0;
            initToolbar("Permohonan Kredit");
        } else
            super.onBackPressed();
    }

    void initToolbar(String title) {
        TextView tv = findViewById(R.id.tvTitle);
        tv.setText(title);
    }

    void initView(){

        llScroll = findViewById(R.id.llScroll);
        progressBar = findViewById(R.id.d_progress);

        llMain = findViewById(R.id.llMain);
        llPengajuan = findViewById(R.id.llPengajuan);
        llSimulasi = findViewById(R.id.llSimulasi);
        llPemohon = findViewById(R.id.llPemohon);
        tvPemohon = findViewById(R.id.tvPemohon);
        llKTP = findViewById(R.id.llKTP);
        tvKTP = findViewById(R.id.tvKTP);

        etNama = findViewById(R.id.etNama);
        etAlamat= findViewById(R.id.etAlamatRumah);
        tvKota = findViewById(R.id.tvKotaP);
        etZipcode = findViewById(R.id.etPZipcode);
        etNoHp = findViewById(R.id.etNoHP);
        etNoHp = findViewById(R.id.etNoHP);
        etPOB = findViewById(R.id.etPOB);
        etDOB = findViewById(R.id.etDOB);
        tvDOB = findViewById(R.id.tvDOB);

        etNoKTP = findViewById(R.id.etNoKTP);
        etTglExpKTP = findViewById(R.id.etTglExpKTP);
        tvTglExpKTP = findViewById(R.id.tvTglExpKTP);
        etAlamatKTP= findViewById(R.id.etAlamatKTP);
        tvKotaKTP = findViewById(R.id.tvKotaKTP);
        etZipcodeKTP = findViewById(R.id.etZipcode);

        cbAlamat = findViewById(R.id.cbAlamatKTP);
        cbTgl = findViewById(R.id.cbTglKTP);

        spinStatus = findViewById(R.id.spinStatus);
        spinSex = findViewById(R.id.spinSex);
        spinCitizen = findViewById(R.id.spinCitizen);

        radioGroup = findViewById(R.id.rgTypeBy);
        rgSuku = findViewById(R.id.rgType);

        etPlafond = findViewById(R.id.etPlafond);
        etInstallment = findViewById(R.id.etInstallment);

        etJangkaWaktu = findViewById(R.id.etJangkaWaktu);
        etSukuBunga = findViewById(R.id.etBunga);

        mTableView = findViewById(R.id.tableview);

    }

    void initOnclick(){

        tvPemohon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHide(llPemohon, tvPemohon);
            }
        });

        tvKTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHide(llKTP, tvKTP);
            }
        });

        etZipcode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    searchZipCode(v);
                    return true;
                }
                return false;
            }
        });

        etZipcodeKTP.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    searchZipCodeKTP(v);
                    return true;
                }
                return false;
            }
        });

        if (sessionManager.getMARITAL() != null){
            MaritalResponse response = MaritalResponse.create(sessionManager.getMARITAL());
            dataMaritals = response.getDataMarital();
            ArrayList<String> list = new ArrayList<>();

            for (int i = 0; i < dataMaritals.size(); i++) {
                list.add(dataMaritals.get(i).getMARITALDESC());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinStatus.setAdapter(dataAdapter);
        }
        
        spinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataMaritals.size() > 0 && position >= 0){
                    idMarital = dataMaritals.get(position).getMARITALID();
                    spinStatus.setEnableErrorLabel(false);
                }else {
                    idMarital = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (sessionManager.getSEX() != null){
            SexResponse response = SexResponse.create(sessionManager.getSEX());
            dataSexes = response.getDataSex();
            ArrayList<String> list = new ArrayList<>();

            for (int i = 0; i < dataSexes.size(); i++) {
                list.add(dataSexes.get(i).getSEXDESC());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinSex.setAdapter(dataAdapter);
        }
        spinSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataSexes.size() > 0 && position >= 0){
                    idSex = dataSexes.get(position).getSEXID();
                    spinSex.setEnableErrorLabel(false);
                }else {
                    idSex = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (sessionManager.getCITIZEN() != null){
            CitizenShipResponse response = CitizenShipResponse.create(sessionManager.getCITIZEN());
            dataCitizens = response.getDataCitizen();
            ArrayList<String> list = new ArrayList<>();

            for (int i = 0; i < dataCitizens.size(); i++) {
                list.add(dataCitizens.get(i).getCITIZENDESC());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinCitizen.setAdapter(dataAdapter);
        }
        spinCitizen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (dataCitizens.size() > 0 && position >= 0){
                    idCitizen = dataCitizens.get(position).getCITIZENID();
                    Log.i(TAG, "onItemSelected: Citizen "+idCitizen);
                    spinCitizen.setEnableErrorLabel(false);
                } else {
                    idCitizen = "";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cbTgl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    tvTglExpKTP.setVisibility(View.GONE);
                    etTglExpKTP.setText("2040-01-01");
                } else {
                    tvTglExpKTP.setVisibility(View.VISIBLE);
                    tvTglExpKTP.setText("");
                    etTglExpKTP.setText("");
                }
            }
        });

        cbAlamat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    etAlamatKTP.setText(etAlamat.getText().toString());
                    etAlamatKTP.setEnabled(false);
                    tvKotaKTP.setText(tvKota.getText().toString());
                    etZipcodeKTP.setText(etZipcode.getText().toString());
                    etZipcodeKTP.setEnabled(false);
                    sCityKTP = sCity;
                    sAddr3KTP = sAddr3;
                } else {
                    etAlamatKTP.setText(etAlamat.getText().toString());
                    etAlamatKTP.setEnabled(true);
                    tvKotaKTP.setText(tvKota.getText().toString());
                    etZipcodeKTP.setText(etZipcode.getText().toString());
                    etZipcodeKTP.setEnabled(true);
                    sCityKTP = "";
                    sAddr3KTP = "";
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbPlafond){
                    etPlafond.setEnabled(true);
                    etInstallment.setEnabled(false);
                    etInstallment.setText("0");
                    etPlafond.requestFocus();
                }
                if (checkedId == R.id.rbInstallment){
                    etPlafond.setEnabled(false);
                    etPlafond.setText("0");
                    etInstallment.setEnabled(true);
                    etInstallment.requestFocus();
                }
            }
        });
    }

    public void doAjukan(View v){

        if (etNama.getText().toString().equals("")){
            setError(etNama);
            llScroll.smoothScrollTo(0, 0);
        } else if (etAlamat.getText().toString().equals("")){
            setError(etAlamat);
            llScroll.smoothScrollTo(0, 0);
        } else if (etZipcode.getText().toString().equals("")){
            setError(etZipcode);
            llScroll.smoothScrollTo(0, 0);
        } else if (etNoHp.getText().toString().equals("")){
            setError(etNoHp);
            llScroll.smoothScrollTo(0, 0);
        } else if (etPOB.getText().toString().equals("")){
            setError(etPOB);
            llScroll.smoothScrollTo(0, 0);
        } else if (etDOB.getText().toString().equals("")){
            setError(tvDOB);
            llScroll.smoothScrollTo(0, 0);
        } else if (idMarital.equals("")){
            spinStatus.setEnableErrorLabel(true);
            spinStatus.setError("Mohon dipilih");
        } else if (idSex.equals("")){
            spinSex.setEnableErrorLabel(true);
            spinSex.setError("Mohon dipilih");
        } else if (idCitizen.equals("")){
            spinCitizen.setEnableErrorLabel(true);
            spinCitizen.setError("Mohon dipilih");
        } else if (etNoKTP.getText().toString().equals("")){
            setErrorKTP(etNoKTP);
//            llScroll.smoothScrollTo(0, 0);
            Log.e(TAG, "doAjukan: ERROR NOKTP");
        } else if (etTglExpKTP.getText().toString().equals("")){
            setErrorKTP(tvTglExpKTP);
//            llScroll.smoothScrollTo(0, 0);
            Log.e(TAG, "doAjukan: ERROR TGLKTP");
        } else if (etAlamatKTP.getText().toString().equals("")){
            setErrorKTP(etAlamatKTP);
//            llScroll.smoothScrollTo(0, 0);
            Log.e(TAG, "doAjukan: ERROR ALAMAT KTP");
        } else if (etZipcodeKTP.getText().toString().equals("")){
            setErrorKTP(etZipcodeKTP);
//            llScroll.smoothScrollTo(0, 0);
            Log.e(TAG, "doAjukan: ERROR ZIPCODE KTP");
        } else {
            Log.i(TAG, "doAjukan: KIRIM");
            showProgress(true);
            CustomerEntry customerEntry = new CustomerEntry("", etNama.getText().toString().trim(), "", "",
                    etAlamat.getText().toString().trim(),"", sAddr3, sCity, etZipcode.getText().toString().trim(),
                    "1", 12,2019, "021", etNoHp.getText().toString().trim(),
                    "0", "0", "0", etPOB.getText().toString().trim(), etDOB.getText().toString().trim(),
                    idMarital, idSex, idCitizen, 0,"1234567890", etNoKTP.getText().toString().trim(),
                    etTglExpKTP.getText().toString().trim(), etAlamatKTP.getText().toString().trim(),
                    "", sAddr3KTP, sCityKTP, etZipcodeKTP.getText().toString().trim(),"1");
            restAPI.postCustomer(customerEntry).enqueue(new Callback<CustomerEntryResponse>() {
                @Override
                public void onResponse(Call<CustomerEntryResponse> call, Response<CustomerEntryResponse> response) {
                    showProgress(false);
                    Log.i(TAG, "onResponse: H " + response.headers().toString());
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.code() == 400) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            CustomerEntryResponse  mError = gson.fromJson(response.errorBody().string(), CustomerEntryResponse.class);
                            if (mError.getResponseText() != null) {
                                if (mError.getResponseText().contains("ID CARD NUMBER")){
                                    DoDialogC("No. KTP sudah didaftarkan sebelumnya");
                                } else
                                    DoDialogC(mError.getResponseText());
                            } else
                                DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                            Log.v("Error code 400",response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: B " + response.body().toString());
                            if (response.body().getResponse().equals("1")) {
                                DoDialog("Data Pengajuan Kredit behasil disimpan");
                            } else {
                                DoDialogC(response.body().getResponseText());
                            }
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                    }
                }

                @Override
                public void onFailure(Call<CustomerEntryResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                    showProgress(false);
                }
            });
        }
    }

    public void doHitung(View v){
        if (etPlafond.getText().toString().equals("") ||
            etInstallment.getText().toString().equals("")){
            DoDialogC("Mohon pilih plafond atau angsuran");
        } else if (etJangkaWaktu.getText().toString().equals("") || etJangkaWaktu.getText().toString().equals("0")){
            DoDialogC("Mohon isi jangka waktu lebih dari 0");
        } else if (etSukuBunga.getText().toString().equals("")) {
            DoDialogC("Mohon isi suku bunga");
        } else {

            ShowProgressDialog("Mensimulasi");
            int calType = 0, bungaType = 0;
            int selectedId = radioGroup.getCheckedRadioButtonId();
            if (selectedId == R.id.rbPlafond)
                calType = 1;
            else
                calType = 2;

            int selectedIds = radioGroup.getCheckedRadioButtonId();
            if (selectedIds == R.id.rbFlat)
                bungaType = 1;
            else
                bungaType= 2;

            SimulasiEntry simulasiEntry = new SimulasiEntry(calType, bungaType, Integer.parseInt(etPlafond.getText().toString()),
                    Integer.parseInt(etInstallment.getText().toString()), Double.parseDouble(etSukuBunga.getText().toString()),
                    Integer.parseInt(etJangkaWaktu.getText().toString()));

            restAPI.postSimulasi(simulasiEntry).enqueue(new Callback<SimulasiResponse>() {
                @Override
                public void onResponse(Call<SimulasiResponse> call, Response<SimulasiResponse> response) {
                    HideProgressDialog();
                    Log.i(TAG, "onResponse: H " + response.headers().toString());
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.code() == 400) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            SimulasiResponse  mError = gson.fromJson(response.errorBody().string(), SimulasiResponse.class);
                            if (mError.getResponseText() != null) {
//                                if (mError.getResponseText().contains("ID CARD NUMBER")){
//                                    DoDialogC("No. KTP sudah didaftarkan sebelumnya");
//                                } else
                                DoDialogC(mError.getResponseText());
                            } else
                                DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                            Log.v("Error code 400",response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: B " + response.body().toString());
                            if (response.body().getResponse().equals("1")) {
                                if (response.body().getDataSimulasi().size() > 0)
                                    if (response.body().getDataSimulasi().get(0).getTablesSimulasi().size() > 0) {
                                        Intent inte = new Intent(MainActivity.this, SimulasiActivity.class);
                                        inte.putExtra("Datas", response.body());
                                        startActivity(inte);
//                                        initializeTableView(response.body().getDataSimulasi().get(0).getTablesSimulasi(),
//                                                String.valueOf(response.body().getDataSimulasi().get(0).getJumlAngsPokok()),
//                                                String.valueOf(response.body().getDataSimulasi().get(0).getJumlAngsBunga()),
//                                                String.valueOf(response.body().getDataSimulasi().get(0).getJumlTotAngsuran())
//                                        );
                                    }
                            } else {
                                DoDialogC(response.body().getResponseText());
                            }
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                    } else
                        DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                }

                @Override
                public void onFailure(Call<SimulasiResponse> call, Throwable t) {
                    HideProgressDialog();
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                    showProgress(false);
                }
            });
        }
    }

    public void doPengajuan(View v){
        llMain.setVisibility(View.GONE);
        llPengajuan.setVisibility(View.VISIBLE);
//        llPengajuan.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
        initToolbar("Pengajuan Kredit");
        back = 1;
    }

    public void doSimulasi(View v){
//        DoDialogC("Sedang dalam perbaikkan. . .");
        llMain.setVisibility(View.GONE);
        llSimulasi.setVisibility(View.VISIBLE);
//        llPengajuan.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
        initToolbar("Simulasi Kredit");
        back = 2;
    }


    void getMarital(){
        restAPI.getMarital().enqueue(new Callback<MaritalResponse>() {
            @Override
            public void onResponse(Call<MaritalResponse> call, Response<MaritalResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    if (response.body().getDataMarital() != null){
                        if (response.body().getDataMarital().size() > 0){

                            dataMaritals = response.body().getDataMarital();
                            ArrayList<String> list = new ArrayList<>();

                            for (int i = 0; i < dataMaritals.size(); i++) {
                                list.add(dataMaritals.get(i).getMARITALDESC());
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                                    android.R.layout.simple_spinner_item, list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinStatus.setAdapter(dataAdapter);

                            if (sessionManager.getMARITAL() == null){
                                sessionManager.saveDataMarital(response.body().serialize());
                            } else {
                                Log.i(TAG, "onResponse getMarital: Check data if not same");
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MaritalResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                if (t.getMessage().contains("timeout") || t.getMessage().contains("timed out"))
                    getMarital();
            }
        });
    }

    void getSex(){
        restAPI.getSex().enqueue(new Callback<SexResponse>() {
            @Override
            public void onResponse(Call<SexResponse> call, Response<SexResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    if (response.body().getDataSex() != null){
                        if (response.body().getDataSex().size() > 0){

                            dataSexes = response.body().getDataSex();
                            ArrayList<String> list = new ArrayList<>();

                            for (int i = 0; i < dataSexes.size(); i++) {
                                list.add(dataSexes.get(i).getSEXDESC());
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                                    android.R.layout.simple_spinner_item, list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinSex.setAdapter(dataAdapter);

                            if (sessionManager.getSEX() == null){
                                sessionManager.saveDataSex(response.body().serialize());
                            } else {
                                Log.i(TAG, "onResponse getSex: Check data if not same");
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SexResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                if (t.getMessage().contains("timeout") || t.getMessage().contains("timed out"))
                    getSex();
            }
        });
    }

    void getCitizen(){
        restAPI.getCitizen().enqueue(new Callback<CitizenShipResponse>() {
            @Override
            public void onResponse(Call<CitizenShipResponse> call, Response<CitizenShipResponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    if (response.body().getDataCitizen() != null){
                        if (response.body().getDataCitizen().size() > 0){

                            dataCitizens = response.body().getDataCitizen();
                            ArrayList<String> list = new ArrayList<>();

                            for (int i = 0; i < dataCitizens.size(); i++) {
                                list.add(dataCitizens.get(i).getCITIZENDESC());
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MainActivity.this,
                                    android.R.layout.simple_spinner_item, list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinCitizen.setAdapter(dataAdapter);
                            if (sessionManager.getCITIZEN() == null){
                                sessionManager.saveDataCitizen(response.body().serialize());
                            } else {
                                Log.i(TAG, "onResponse getCitizen: Check data if not same");
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CitizenShipResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                if (t.getMessage().contains("timeout") || t.getMessage().contains("timed out"))
                    getCitizen();
            }
        });
    }

    public void searchZipCode(View v){
        hideSoftKeyboard();
        ShowProgressDialog("Searching data. . .");
        PostZipcode PostZipcode = new PostZipcode(etZipcode.getText().toString());
        restAPI.postZipcode(PostZipcode).enqueue(new Callback<ZipcodeReponse>() {
            @Override
            public void onResponse(Call<ZipcodeReponse> call, Response<ZipcodeReponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null){
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    ZipcodeReponse zipcodeReponse = response.body();
                    if (zipcodeReponse.getDataZipcode() != null){
                        if (zipcodeReponse.getDataZipcode().size() > 0){
                            dataZipcodes = zipcodeReponse.getDataZipcode();
                            tvKota.setText(zipcodeReponse.getDataZipcode().get(0).getCITYNAME());
                            sAddr3 = dataZipcodes.get(0).getDESCRIPTION();
                            sCity = dataZipcodes.get(0).getCITYID();
//                            etNoHp.requestFocus();
                        }
                        DoDialogC(zipcodeReponse.getResponseText());
                    }
                }
                HideProgressDialog();
            }

            @Override
            public void onFailure(Call<ZipcodeReponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                HideProgressDialog();
                DoDialogC("Koneksi bermasalah, silahkan coba lagi");
            }
        });
    }

    public void searchZipCodeKTP(View v){
        hideSoftKeyboard();
        ShowProgressDialog("Searching data. . .");
        PostZipcode PostZipcode = new PostZipcode(etZipcodeKTP.getText().toString());
        restAPI.postZipcode(PostZipcode).enqueue(new Callback<ZipcodeReponse>() {
            @Override
            public void onResponse(Call<ZipcodeReponse> call, Response<ZipcodeReponse> response) {
                Log.i(TAG, "onResponse: H " + response.headers().toString());
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null){
                    Log.i(TAG, "onResponse: B " + response.body().toString());
                    ZipcodeReponse zipcodeReponse = response.body();
                    if (zipcodeReponse.getDataZipcode() != null){
                        if (zipcodeReponse.getDataZipcode().size() > 0){
                            dataZipcodes = zipcodeReponse.getDataZipcode();
                            tvKotaKTP.setText(zipcodeReponse.getDataZipcode().get(0).getCITYNAME());
                            sAddr3KTP = dataZipcodes.get(0).getDESCRIPTION();
                            sCityKTP = dataZipcodes.get(0).getCITYID();
                        }
                        DoDialogC(zipcodeReponse.getResponseText());
                    }
                }
                HideProgressDialog();
            }

            @Override
            public void onFailure(Call<ZipcodeReponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                HideProgressDialog();
                DoDialogC("Koneksi bermasalah, silahkan coba lagi");
            }
        });
    }

    private void initializeTableView(List<TablesSimulasi> tablesSimulasis, String a, String b, String c) {

        // Create TableView Adapter
        mTableViewAdapter = new MyTableViewAdapter(this);

        mTableView.setAdapter(mTableViewAdapter);

        // Create an instance of a Filter and pass the TableView.
        //mTableFilter = new Filter(mTableView);
        List<RowHeader> listRowHeader = new ArrayList<>();
        for (int i = 0; i < tablesSimulasis.size(); i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "" + i);
            listRowHeader.add(header);
        }
        RowHeader headerbot = new RowHeader("1", "JUMLAH");
        listRowHeader.add(headerbot);

        List<ColumnHeader> listColumnHeader = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            String title = "";
            if (i == 0)
                title = "Angsuran Pokok";
            if (i == 1)
                title = "Angsuran Bunga";
            if (i == 2)
                title = "Total Angsuran";
            if (i == 3)
                title = "Sisa Pinjaman";
            ColumnHeader header = new ColumnHeader(String.valueOf(i), title);
            listColumnHeader.add(header);
        }

        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < tablesSimulasis.size(); i++) {
            List<Cell> cellList = new ArrayList<>();

            for (int j = 0; j < 4; j++) {
                String text = "";
                if (j == 0) text = String.valueOf(tablesSimulasis.get(i).getSISAPINJAMAN());
                if (j == 1) text = String.valueOf(tablesSimulasis.get(i).getANGSURANBUNGA());
                if (j == 2) text = String.valueOf(tablesSimulasis.get(i).getTOTALANGSURAN());
                if (j == 3) text = String.valueOf(tablesSimulasis.get(i).getSISAPINJAMAN());
                String id = i+"";
                Cell cell = new Cell(id, text);
                cellList.add(cell);
            }
            list.add(cellList);
        }
        List<Cell> cellList = new ArrayList<>();

        for (int j = 0; j < 4; j++) {
            String text = "";
            if (j == 0) text = a;
            if (j == 1) text = b;
            if (j == 2) text = c;
            if (j == 3) text = "";
            String id = "1";
            Cell cell = new Cell(id, text);
            cellList.add(cell);
        }
        list.add(cellList);

        // Load the dummy data to the TableView
        mTableViewAdapter.setAllItems(listColumnHeader, listRowHeader, list);


    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void dialogDatePickerLight(View v) {
        Calendar cur_calender = Calendar.getInstance();
        cur_calender.set(Calendar.YEAR, 1960);
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        etDOB.setText(Method.getFormattedDate(date_ship_millis));
                        tvDOB.setText(Method.getFormattedDateShow(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark light
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    public void dialogDatePickerKTP(View v) {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        etTglExpKTP.setText(Method.getFormattedDate(date_ship_millis));
                        tvTglExpKTP.setText(Method.getFormattedDateShow(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark light
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.show(getFragmentManager(), "Datepickerdialog");
    }

    public void showKota(View v){
        DoDialogC("Isian otomatis - Silakan masukkan Kode Pos di bawah");
    }

    void setError(EditText et){
        et.requestFocus();
        et.setError("Harus diisi!");
        if (!llPemohon.isShown()) {
            llPemohon.setVisibility(View.VISIBLE);
            tvPemohon.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_remove_circle_outline_white_24dp, 0);
        }
    }

    void setError(TextView et){
        et.requestFocus();
        et.setError("Harus diisi!");
        if (!llPemohon.isShown()) {
            llPemohon.setVisibility(View.VISIBLE);
            tvPemohon.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_remove_circle_outline_white_24dp, 0);
        }
    }

    void setErrorKTP(EditText et){
        et.requestFocus();
        et.setError("Harus diisi!");
        if (!llKTP.isShown()) {
            llKTP.setVisibility(View.VISIBLE);
            tvKTP.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_remove_circle_outline_white_24dp, 0);
        }
    }

    void setErrorKTP(TextView et){
        et.requestFocus();
        et.setError("Harus diisi!");
        if (!llKTP.isShown()) {
            llKTP.setVisibility(View.VISIBLE);
            tvKTP.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_remove_circle_outline_white_24dp, 0);
        }
    }

    void showHide(View v, TextView tv){
        if (v.isShown()){
            v.setVisibility(View.GONE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_add_circle_outline_white_24dp, 0);
        } else {
            v.setVisibility(View.VISIBLE);
            tv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_remove_circle_outline_white_24dp, 0);
        }
    }

    void showProgress(boolean shows){
        llScroll.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }

    public void DoDialog(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                }).show();
    }

    public void DoDialogC(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
    }

    public void ShowProgressDialog(String text) {
        dialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        TextView tv = dialogView.findViewById(R.id.textViewDialog);
        tv.setText(text);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }
}
