package com.apps.c2ls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.apps.c2ls.Adapter.MyTableViewAdapter;
import com.apps.c2ls.Model.Cell;
import com.apps.c2ls.Model.ColumnHeader;
import com.apps.c2ls.Model.RowHeader;
import com.apps.c2ls.Model.SimulasiResponse;
import com.apps.c2ls.Model.TablesSimulasi;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SimulasiActivity extends AppCompatActivity {

    private static final String TAG = "SimulasiActivity";
    TextView tvPlafond, tvInstallment, tvWaktu, tvBunga;
    SimulasiResponse simulasiReponse;
    private AbstractTableAdapter mTableViewAdapter;
    private TableView mTableView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulasi);
        simulasiReponse = getIntent().getParcelableExtra("Datas");

        InitView();

        initializeTableView(simulasiReponse.getDataSimulasi().get(0).getTablesSimulasi(),
                String.valueOf(simulasiReponse.getDataSimulasi().get(0).getJumlAngsPokok()),
                String.valueOf(simulasiReponse.getDataSimulasi().get(0).getJumlAngsBunga()),
                String.valueOf(simulasiReponse.getDataSimulasi().get(0).getJumlTotAngsuran())
        );

        tvPlafond.setText("Plafond " + getFormatIDR(String.valueOf(simulasiReponse.getDataSimulasi().get(0).getPlafond())));
        tvInstallment.setText("Angsuran " + getFormatIDR(String.valueOf(simulasiReponse.getDataSimulasi().get(0).getInstallment())));
        tvWaktu.setText("Jangka Waktu " + simulasiReponse.getDataSimulasi().get(0).getTimePeriod() + " bulan");
        if (simulasiReponse.getDataSimulasi().get(0).getInterestRateType() == 1)
            tvBunga.setText("Suku bunga " + simulasiReponse.getDataSimulasi().get(0).getInterestRate() + "% per tahun / Flat");
        else
            tvBunga.setText("Suku bunga " + simulasiReponse.getDataSimulasi().get(0).getInterestRate() + "% per tahun / Anuitas");
    }

    void InitView() {
        tvPlafond = findViewById(R.id.tvPlafond);
        tvInstallment = findViewById(R.id.tvInstallment);
        tvWaktu = findViewById(R.id.tvWaktu);
        tvBunga = findViewById(R.id.tvBunga);
        mTableView = findViewById(R.id.tableview);
    }

    private void initializeTableView(List<TablesSimulasi> tablesSimulasis, String a, String b, String c) {

        Log.w(TAG, "initializeTableView: Size "+tablesSimulasis.size() );
        // Create TableView Adapter
        mTableViewAdapter = new MyTableViewAdapter(this);

        mTableView.setAdapter(mTableViewAdapter);

        // Create an instance of a Filter and pass the TableView.
        //mTableFilter = new Filter(mTableView);
        List<RowHeader> listRowHeader = new ArrayList<>();
        for (int i = 0; i < tablesSimulasis.size(); i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "" + i);
            listRowHeader.add(header);
        }
        RowHeader headerbot = new RowHeader("1", "JUMLAH");
        listRowHeader.add(headerbot);

        List<ColumnHeader> listColumnHeader = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            String title = "";
            if (i == 0)
                title = "Angsuran Pokok";
            if (i == 1)
                title = "Angsuran Bunga";
            if (i == 2)
                title = "Total Angsuran";
            if (i == 3)
                title = "Sisa Pinjaman";
            ColumnHeader header = new ColumnHeader(String.valueOf(i), title);
            listColumnHeader.add(header);
        }

        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < tablesSimulasis.size(); i++) {
            List<Cell> cellList = new ArrayList<>();

            for (int j = 0; j < 4; j++) {
                String text = "";
                if (j == 0) text = getFormatIDR(String.valueOf(tablesSimulasis.get(i).getSISAPINJAMAN()));
                if (j == 1) text = getFormatIDR(String.valueOf(tablesSimulasis.get(i).getANGSURANBUNGA()));
                if (j == 2) text = getFormatIDR(String.valueOf(tablesSimulasis.get(i).getTOTALANGSURAN()));
                if (j == 3) text = getFormatIDR(String.valueOf(tablesSimulasis.get(i).getSISAPINJAMAN()));
                String id = i + "";
                Cell cell = new Cell(id, text);
                cellList.add(cell);
            }
            list.add(cellList);
        }
        List<Cell> cellList = new ArrayList<>();

        for (int j = 0; j < 4; j++) {
            String text = "";
            if (j == 0) text = getFormatIDR(a);
            if (j == 1) text = getFormatIDR(b);
            if (j == 2) text = getFormatIDR(c);
            if (j == 3) text = "";
            String id = "1";
            Cell cell = new Cell(id, text);
            cellList.add(cell);
        }
        list.add(cellList);

        // Load the dummy data to the TableView
        mTableViewAdapter.setAllItems(listColumnHeader, listRowHeader, list);


    }

    public static String getFormatIDR(String r) {
        Double a = Double.parseDouble(r);
        Locale local = new Locale("EN", "EN");
        final DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance(local);
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
//
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);


        return kursIndonesia.format(a);//.replace(",00", "");
    }
}
