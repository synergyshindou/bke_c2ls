package com.apps.c2ls.Holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.c2ls.R;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

/**
 * Created by Dell_Cleva on 15/04/2019.
 */

public class RowHeaderViewHolder extends AbstractViewHolder {
    public final TextView row_header_textview;
    public final LinearLayout cell_container;

    public RowHeaderViewHolder(View itemView) {
        super(itemView);
        row_header_textview = itemView.findViewById(R.id.row_header_textview);
        cell_container = itemView.findViewById(R.id.row_header_container);
    }
}
