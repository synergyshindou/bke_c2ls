package com.apps.c2ls.Holder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.c2ls.Model.Cell;
import com.apps.c2ls.R;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

/**
 * Created by Dell_Cleva on 15/04/2019.
 */

public class CellViewHolder extends AbstractViewHolder {

    private static final String TAG = "CellViewHolder";
    public final TextView cell_textview;
    public final RelativeLayout cell_container;
    public final LinearLayout cell_containerz;
    private Cell cell;

    public CellViewHolder(View itemView) {
        super(itemView);
        cell_textview = itemView.findViewById(R.id.cell_data);
        cell_container = itemView.findViewById(R.id.cell_container);
        cell_containerz = itemView.findViewById(R.id.cell_containerz);
    }

    public void setCell(Cell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        if (Integer.parseInt(cell.getId()) % 2 == 0) {
            Log.w(TAG, "setCellColor: "+cell.getId());
            cell_containerz.setBackgroundColor(Color.parseColor("#eff3ff"));
        } else {
            cell_containerz.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
        cell_textview.requestLayout();
    }
}
