package com.apps.c2ls;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.c2ls.Model.GlobalResponse;
import com.apps.c2ls.Model.LoginEntry;
import com.apps.c2ls.Model.LoginResponse;
import com.apps.c2ls.Model.RegisterEntry;
import com.apps.c2ls.Rest.ApiConfig;
import com.apps.c2ls.Rest.AppConfig;
import com.apps.c2ls.Utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    ProgressBar progressBar;
    View formLogin, formRegister;
    int back = 0;
    TextInputEditText etEmail, etPass, etUser, etEmailD, etPassD, etPassDR;
    ApiConfig restAPI;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(this);
        initToolbar("Login");
        initView();

        restAPI = AppConfig.getRetrofit().create(ApiConfig.class);
    }

    void initToolbar(String title) {
        TextView tv = findViewById(R.id.tvTitle);
        tv.setText(title);
    }

    void initView() {

        progressBar = findViewById(R.id.login_progress);
        formLogin = findViewById(R.id.login_form);
        formRegister = findViewById(R.id.register_form);

        etEmail = findViewById(R.id.etEmail);
        etPass = findViewById(R.id.etPass);
        etUser = findViewById(R.id.etUser);
        etEmailD = findViewById(R.id.etEmailD);
        etPassD = findViewById(R.id.etPassD);
        etPassDR = findViewById(R.id.etPassDR);
    }

    @Override
    public void onBackPressed() {
        if (back == 1) {
            formLogin.setVisibility(View.VISIBLE);
            formRegister.setVisibility(View.GONE);
            back = 0;
        } else
            super.onBackPressed();
    }

    public void doRegister(View v) {

        formLogin.setVisibility(View.GONE);
        formRegister.setVisibility(View.VISIBLE);
        back = 1;
    }

    public void onLogin(View v) {
        if (etEmail.getText().toString().equals("")) {
            etEmail.setError("Mohon di isi!");
            etEmail.requestFocus();
        } else if (etPass.getText().toString().equals("")) {
            etPass.setError("Mohon di isi!");
            etPass.requestFocus();
        } else {
            showProgress(true);
            LoginEntry loginEntry = new LoginEntry();
            loginEntry.setUsername(etEmail.getText().toString().trim());
            loginEntry.setPassword(etPass.getText().toString().trim());

            restAPI.login(loginEntry).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.i(TAG, "onResponse: H " + response.headers().toString());
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.code() == 400) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            LoginResponse mError = gson.fromJson(response.errorBody().string(), LoginResponse.class);
                            if (mError.getResponseText() != null) {
                                DoDialogC(mError.getResponseText());
                            } else
                                DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                            Log.v("Error code 400", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: B " + response.body().toString());
                            if (response.body().getResponse().equals("1")) {
                                if (response.body().getDataLogin() != null){
                                    if (response.body().getDataLogin().size() > 0){
//                                        if (response.body().getDataLogin().get(0).getGroupId().equals("9999999999")) {
                                            sessionManager.saveToken(response.body().getDataLogin().get(0).getTransactionToken());
                                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                            finish();
//                                        }
                                        
                                    }
                                }
                            } else {
                                DoDialogC(response.body().getResponseText());
                            }
                        } else
                            DoDialogC("Koneksi bermasalah, silahkan coba lagi");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    DoDialogC("Tidak terhubung dengan internet, silahkan coba lagi.");
                    showProgress(false);
                }
            });
        }
    }

    public void doApplyRegister(View v) {
        if (etUser.getText().toString().equals("")) {
            etUser.setError("Mohon di isi!");
            etUser.requestFocus();
        }if (etEmailD.getText().toString().equals("")) {
            etEmailD.setError("Mohon di isi!");
            etEmailD.requestFocus();
        } else if (etPassD.getText().toString().equals("")) {
            etPassD.setError("Mohon di isi!");
            etPassD.requestFocus();
        } else if (etPassDR.getText().toString().equals("")) {
            etPassDR.setError("Mohon di isi!");
            etPassDR.requestFocus();
        } else if (!etPassDR.getText().toString().equals(etPassD.getText().toString())) {
            etPassDR.setError("Password Tidak sama!");
            etPassDR.requestFocus();
        } else {
            Log.i(TAG, "doApplyRegister: SAVE");
            showProgressD(true);
            RegisterEntry registerEntry = new RegisterEntry(etUser.getText().toString().trim(),
                    etEmailD.getText().toString().trim(),
                    etPassD.getText().toString().trim(),
                    etPassDR.getText().toString().trim());

            restAPI.registerAccount(registerEntry).enqueue(new Callback<GlobalResponse>() {
                @Override
                public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                    Log.i(TAG, "onResponse: H " + response.headers().toString());
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.code() == 400) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            LoginResponse mError = gson.fromJson(response.errorBody().string(), LoginResponse.class);
                            if (mError.getResponseText() != null) {
                                DoDialogD(mError.getResponseText());
                            } else
                                DoDialogD("Koneksi bermasalah, silahkan coba lagi");
                            Log.v("Error code 400", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (response.code() == 200) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: B " + response.body().toString());
                            if (response.body().getResponse().equals("1")) {
                                DoDialog(response.body().getResponseText());
                            } else {
                                DoDialogD(response.body().getResponseText());
                            }
                        } else
                            DoDialogD("Koneksi bermasalah, silahkan coba lagi");
                    }
                }

                @Override
                public void onFailure(Call<GlobalResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    DoDialogD("Tidak terhubung dengan internet, silahkan coba lagi.");
                    showProgressD(false);
                }
            });
        }
    }

    void showProgress(boolean shows) {
        formLogin.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }

    void showProgressD(boolean shows) {
        formRegister.setVisibility(shows ? View.GONE : View.VISIBLE);
        progressBar.setVisibility(shows ? View.VISIBLE : View.GONE);
    }

    public void DoDialogC(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showProgress(false);
                    }
                }).show();
    }

    public void DoDialogD(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showProgressD(false);
                    }
                }).show();
    }

    public void DoDialog(String str) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showProgressD(false);
                        onBackPressed();
                        etUser.setText("");
                        etEmailD.setText("");
                        etPassD.setText("");
                        etPassDR.setText("");
                    }
                }).show();
    }
}
