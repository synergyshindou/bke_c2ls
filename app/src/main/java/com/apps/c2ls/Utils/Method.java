package com.apps.c2ls.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dell_Cleva on 11/04/2019.
 */

public class Method {

    public static String getFormattedDate(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        return newFormat.format(new Date(dateTime));
    }

    public static String getFormattedDateShow(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMMM yyyy");
        return newFormat.format(new Date(dateTime));
    }
}
