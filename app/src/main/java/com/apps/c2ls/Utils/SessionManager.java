package com.apps.c2ls.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Dell_Cleva on 15/04/2019.
 */

public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    private SharedPreferences.Editor editor;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    String MARITAL="marital";

    String CITIZEN="citizen";

    String SEX="sex";

    String TOKEN="TOKEN";

    public SessionManager(Context context) {
        String MY_PREFS_NAME = getClass().getPackage().toString();
        pref = context.getSharedPreferences(MY_PREFS_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void saveDataMarital(String data){
        editor.putString(MARITAL, data);
        editor.apply();
    }
    public void saveDataCitizen(String data){
        editor.putString(CITIZEN, data);
        editor.apply();
    }
    public void saveDataSex(String data){
        editor.putString(SEX, data);
        editor.apply();
    }

    public void saveToken(String data){
        editor.putString(TOKEN, data);
        editor.apply();
    }

    public String getMARITAL(){
        return pref.getString(MARITAL, null);
    }
    public String getCITIZEN(){
        return pref.getString(CITIZEN, null);
    }
    public String getSEX(){
        return pref.getString(SEX, null);
    }

    public String getToken(){
        return pref.getString(TOKEN, null);
    }
}
